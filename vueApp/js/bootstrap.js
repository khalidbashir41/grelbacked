
import _ from 'lodash';

import Vue from 'vue';

import VueRouter from 'vue-router';

import Vuetify from 'vuetify';

import j from "jquery";

import axios from 'axios';

import Carousel3d from 'vue-carousel-3d';

import VueStar from 'vue-star'

window._ = _;

window.$ = j;

window.Vue = Vue;

Vue.use(VueRouter);

// Helpers
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
    theme: {
        primary: "#1464bf",
        secondary: "#e57373",
        accent: "#9c27b0",
        error: "#f44336",
        warning: "#ffeb3b",
        info: "#2196f3",
        success: "#4caf50",
        ideaCore: "689f38",
        fbColor: "#3b579d",
        greyText: '#4b4b4b',
        bussInfo: "#263768",
        bussAssess:"#F5A623",
        bussVerb:'#773BAB',
        bussVisual:'#EBB626',
        bussProspect:'#2451B9',
        bussSOI:'#A53BD2',
        formText:'#576b7d',
        bussStoreFront:'#505190',
        bussFace:'#51A65E',
        bussFSBO:'#E0D15F',
        bussAdvertising:'#4C7EF2',
        bussOnline:'#DE5F5F',
        bussPractical:'#9CD92B',
        bussHealth:'#A82939',
        bussRelation:'#62903e'
      }
});
Vue.use(Carousel3d);
Vue.component('VueStar', VueStar);

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = axios;

