var routes = [
    { path: '*', redirect: '/' },
    {
        path: '/',
        component: require('./components/Dashboard'),
        meta: {

        }
    },
    {
        path: '/training/:p/:q',
        component: require('./components/Training'),
        meta: {

        }
    },
    {
        path: '/tools/:p',
        component: require('./components/Tools'),
        meta: {

        }
    },
    {
        path: '/branding/:p',
        component: require('./components/Branding'),
        meta: {

        }
    },
    {
        path: '/scripts/:p',
        component: require('./components/scripts'),
        meta: {

        }
    },
    {
        path: '/businessInfo',
        component: require('./components/businessInfo'),
        meta: {

        }
    },
    {
        path: '/assessment',
        component: require('./components/assesment'),
        meta: {

        }
    },
    {
        path: '/verbalization',
        component: require('./components/verbalization'),
        meta: {

        }
    },
    {
        path: '/visualization',
        component: require('./components/visualization'),
        meta: {

        }
    },
    {
        path: '/prospecting',
        component: require('./components/prospecting'),
        meta: {

        }
    },
    {
        path: '/buildingSOI',
        component: require('./components/buildingSOI'),
        meta: {

        }
    },
    {
        path: '/storeFront',
        component: require('./components/storeFront'),
        meta: {

        }
    },
    {
        path: '/faceToFace',
        component: require('./components/faceToFace'),
        meta: {

        }
    },
    {
        path: '/FSBO',
        component: require('./components/FSBO'),
        meta: {

        }
    },
    {
        path: '/advertising',
        component: require('./components/advertising'),
        meta: {

        }
    },
    {
        path: '/onlineAdvertising',
        component: require('./components/onlineAdvertising'),
        meta: {

        }
    },
    {
        path: '/practicalSummary',
        component: require('./components/practicalSummary'),
        meta: {

        }
    },
    {
        path: '/health',
        component: require('./components/health'),
        meta: {

        }
    },
    {
        path: '/relationship',
        component: require('./components/relationship'),
        meta: {

        }
    },
    {
        path: '/grelForum',
        component: require('./components/grelForum'),
        meta: {

        }
    },

];

import VueRouter from 'vue-router';

export default new VueRouter({
    routes: routes
});